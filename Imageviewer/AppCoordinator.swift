//  
//  Created by Daniel Metzing on 28.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import UIKit

final class AppCoordinator {
    
    private let keyWindow: UIWindow
    private var apiClient: APIClient
    private let postsListViewController: PostsListViewController
    private let loginViewController: LoginViewController
    private let storeProvider: StoreProvider
    private var fullScreenImageViewController: FullScreenImageViewController?
    
    init(window: UIWindow) {
        keyWindow = window
        self.apiClient = APIClient()
        self.storeProvider = StoreProvider()
        self.postsListViewController = PostsListViewController(apiClient: apiClient, storeProvider: storeProvider)
        self.loginViewController = LoginViewController(apiClient: apiClient)
        self.loginViewController.delegate = self
        self.postsListViewController.delegate = self
    }
    
    func start() {
        Theme.setUp(to: .current)
        
        let navigationController = UINavigationController(rootViewController: postsListViewController)
        keyWindow.rootViewController = navigationController
        
        if NetworkConnectivityService.isConnected && self.apiClient.accessToken == nil {
            navigationController.present(loginViewController, animated: true, completion: nil)
        } else {
            postsListViewController.loadViewIfNeeded()
            postsListViewController.presentUsersRecentMedia()
        }
    }
}

extension AppCoordinator: LoginViewControllerDelegate {
    
    func loginViewController(_ loginViewController: LoginViewController, didAuthenticateWithToken token: AccessToken) {
        apiClient.accessToken = token
        loginViewController.dismiss(animated: true, completion: nil)
        postsListViewController.presentUsersRecentMedia()
    }
}

extension AppCoordinator: PostsListViewControllerDelegate {
    
    func postsListViewController(_ postsListViewController: PostsListViewController, didSelectItemWithURL url: URL) {
        let fullScreenImageViewController = FullScreenImageViewController(initialURL: url, storeProvider: storeProvider)
        let navigationController = UINavigationController(rootViewController: fullScreenImageViewController)
        navigationController.modalPresentationStyle = .overCurrentContext
        postsListViewController.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    func postsListViewControllerRequestNewLogin(_ postsListViewController: PostsListViewController) {
        if NetworkConnectivityService.isConnected {
            postsListViewController.navigationController?.present(loginViewController, animated: true, completion: nil)
        }
    }
}

