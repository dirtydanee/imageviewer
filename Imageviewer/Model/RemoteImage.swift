//  
//  Created by Daniel Metzing on 02.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import CoreData
import UIKit

final class RemoteImage: NSManagedObject {
    @NSManaged var path: String
    @NSManaged private var resolutionId: String
    
    enum Resolution: String {
        case low
        case standard
    }
    
    var resolution: Resolution {
        get { return Resolution(rawValue: resolutionId)! }
        set { resolutionId = newValue.rawValue }
    }
}
