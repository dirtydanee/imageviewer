//  
//  Created by Daniel Metzing on 05.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import Foundation
import IGListKit

final class FullScreenImageCellViewModel: NSObject {
    let url: URL
    
    init(url: URL) {
        self.url = url
    }
    
    static func makeViewModel(from post: Post) -> FullScreenImageCellViewModel {
        guard let standardResolutionPath = post.images.first(where: { $0.resolution == .standard }) else {
            fatalError("Unable to fetch standard resolution image")
        }
        
        return FullScreenImageCellViewModel(url: URL(string: standardResolutionPath.path)!)
    }
}

extension FullScreenImageCellViewModel: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return NSString(string: "\(hashValue)")
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let object = object else { return false }
        return self.diffIdentifier().hash == object.diffIdentifier().hash
    }
}
