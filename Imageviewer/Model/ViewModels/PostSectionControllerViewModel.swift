//  
//  Created by Daniel Metzing on 06.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import IGListKit

final class PostSectionControllerViewModel: ListDiffable {
    
    let items: [PostCellViewModel]
    
    init(items: [PostCellViewModel]) {
        self.items = items
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        let idenfitifier = items.reduce(into: "") { (result, viewModel) in
            result += "\(viewModel.hashValue)"
        }
        return NSString(string: idenfitifier)
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let object = object else { return false }
        return self.diffIdentifier().hash == object.diffIdentifier().hash
    }
}
