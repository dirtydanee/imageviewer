//  
//  Created by Daniel Metzing on 03.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import IGListKit

final class PostCellViewModel: NSObject {
    let id: String
    let comments: String
    let likes: String
    let imageURL: URL
    
    init(id: String, comments: Int64, likes: Int64, imageURL: URL) {
        self.id = id
        self.comments = "\(comments)"
        self.likes = "\(likes)"
        self.imageURL = imageURL
    }
    
    static func makeViewModel(from post: Post) -> PostCellViewModel {
        guard let lowResolutionImage = post.images.first(where: { $0.resolution == .low }) else {
            fatalError("Unable to fetch low resolution image")
        }
        return PostCellViewModel(id: post.id, comments: post.comments, likes: post.likes, imageURL: URL(string: lowResolutionImage.path)!)
    }
}
