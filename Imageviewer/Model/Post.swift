//  
//  Created by Daniel Metzing on 02.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//

import CoreData

final class Post: NSManagedObject {
    @NSManaged var id: String
    @NSManaged var createdTime: String
    @NSManaged var comments: Int64
    @NSManaged var likes: Int64
    @NSManaged var images: Set<RemoteImage>
}
