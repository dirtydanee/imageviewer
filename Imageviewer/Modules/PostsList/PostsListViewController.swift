//  
//  Created by Daniel Metzing on 29.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import UIKit
import IGListKit
import CoreData

protocol PostsListViewControllerDelegate: class {
    func postsListViewController(_ postsListViewController: PostsListViewController, didSelectItemWithURL url: URL)
    func postsListViewControllerRequestNewLogin(_ postsListViewController: PostsListViewController)
}

final class PostsListViewController: UIViewController, ActivityDisplayer, AlertDisplayer {
    
    private let viewModel: PostsListViewModel
    private var collectionView: ListCollectionView!
    private var adapter: ListAdapter!
    
    weak var delegate: PostsListViewControllerDelegate?
    
    init(apiClient: APIClient, storeProvider: StoreProvider) {
        viewModel = PostsListViewModel(apiClient: apiClient, storeProvider: storeProvider)
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpCollectionView()
        setUpViewModel()
    }

    func presentUsersRecentMedia() {
        adapter.dataSource = viewModel.listDataSource
        viewModel.loadUserRecentMedia()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        adapter.reloadData(completion: nil)
    }
}

private extension PostsListViewController {
    
    func setUpView() {
        view.backgroundColor = Theme.current.viewController.backgroundColor
        title = Localized.postsTitle
    }
    
    func setUpCollectionView() {
        collectionView = ListCollectionView(frame: .zero)
        collectionView.backgroundColor = .clear
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.pinToSuperview()
        
        let updater = ListAdapterUpdater()
        adapter = ListAdapter(updater: updater, viewController: self)
        adapter.collectionView = collectionView
    }
    
    func setUpViewModel() {
        viewModel.onPostsUpdate = { [weak self] (animated) in
            self?.adapter.performUpdates(animated: animated)
        }
        
        viewModel.onSetActivityState = { [weak self] in
            self?.status = $0
        }
        
        viewModel.onDidSelectItemWithURL = { [weak self] (url) in
            guard let self = self else { return }
            self.delegate?.postsListViewController(self, didSelectItemWithURL: url)
        }
        
        viewModel.onErrorReceivedWhileLoading = { [weak self] in
            guard let self = self else { return }
            self.delegate?.postsListViewControllerRequestNewLogin(self)
        }
        
        viewModel.onShowAlert = { [weak self] alert, animated in
            self?.showAlert(alert, animated: animated)
        }
    }
}
