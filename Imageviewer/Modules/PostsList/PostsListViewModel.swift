//  
//  Created by Daniel Metzing on 29.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import CoreData
import IGListKit

final class PostsListViewModel: NSObject {
    
    private let apiClient: APIClient
    private let storeProvider: StoreProvider
    private let dataSource: PostsListDataSource
    
    enum Error: Swift.Error {
        case missingAPIToken
        case emptyDataSource
    }
    
    var listDataSource: ListAdapterDataSource {
        return dataSource
    }
    
    private var state: State = .loading {
        didSet {
            switch state {
            case .loading:
                 onSetActivityState?(.loading)
            case .loaded:
                onSetActivityState?(.loaded)
                dataSource.fetchPosts()
                onPostsUpdate?(true)
            case .error(let error):
                onSetActivityState?(.loaded)
                let alert = UIAlertController.okAlert(for: error, onOkPress: { [weak self] in self?.onErrorReceivedWhileLoading?() })
                onShowAlert?(alert, true)
            }
        }
    }
    
    var onPostsUpdate: ((_ animated: Bool) -> Void)?
    var onSetActivityState: ((ActivityStatus) -> Void)?
    var onShowAlert: ((UIAlertController, Bool) -> Void)?
    var onDidSelectItemWithURL: ((URL) -> Void)?
    var onErrorReceivedWhileLoading: (() -> Void)?
    
    init(apiClient: APIClient, storeProvider: StoreProvider) {
        self.apiClient = apiClient
        self.storeProvider = storeProvider
        self.dataSource = PostsListDataSource(coreDataStack: storeProvider.coreDataStack)
        super.init()
        self.setUpDataSource()
    }
    
    func loadUserRecentMedia() {
        state = .loading
        switch NetworkConnectivityService.isConnected {
        case true: fetchRemoteContent()
        case false: fetchPersistedContent()
        }
    }
}

private extension PostsListViewModel {
    enum State {
        case loading
        case loaded
        case error(Swift.Error)
    }
}

// MARK: Fetching

private extension PostsListViewModel {
    func fetchRemoteContent() {
        guard let accessToken = apiClient.accessToken else {
            state = .error(Error.missingAPIToken)
            return
        }
        
        let request = InstagramAPI.userRecentMedia(token: accessToken)
        apiClient.execute(target: request) { [weak self] (result: Result<InstagramAPI.Response.UserRecentMedia>) in
            guard let self = self else { return }
            switch result {
            case .success(let response):
                self.makePhotos(from: response)
                self.storeProvider.coreDataStack.saveChanges()
                self.state = .loaded
            case .failure(let error):
                self.state = .error(error)
            }
        }
    }
    
    func fetchPersistedContent() {
        guard !dataSource.isEmpty else {
            self.state = .error(Error.emptyDataSource)
            return
        }
       
       self.state = .loaded
    }
}

private extension PostsListViewModel {
    
    func makePhotos(from response: InstagramAPI.Response.UserRecentMedia) {
        do {

            let transformer = MediaResponseTransformer(postsStore: storeProvider.postsStore, imageStore: storeProvider.imageStore)
            try transformer.insert(response, to: storeProvider.coreDataStack.writeContext)
        } catch {
            state = .error(error)
        }
    }
    
    func setUpDataSource() {
        self.dataSource.onDataSourceNeedsReload = { [weak self, weak dataSource] in
            dataSource?.fetchPosts()
            self?.onPostsUpdate?(true)
        }
        
        self.dataSource.onPostViewModelWasSelected = { [weak self] viewModel in
            self?.onDidSelectItemWithURL?(viewModel.imageURL)
        }
    }
}
