//  
//  Created by Daniel Metzing on 04.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import IGListKit

final class PostsSectionController: ListSectionController {
    
    struct Layout {
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        static let interitemSpacing: CGFloat = 2
    }
    
    let viewModel: PostSectionControllerViewModel
    
    var onItemWasSelected: ((Int) -> Void)?
    
    override func numberOfItems() -> Int {
        return viewModel.items.count
    }
    
    init(viewModel: PostSectionControllerViewModel) {
        self.viewModel = viewModel
        super.init()
        self.inset = Layout.insets
        self.minimumInteritemSpacing = Layout.interitemSpacing
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        let containerWidth = collectionContext!.containerSize.width
        let interitemSpacing = CGFloat(numberOfItems() - 1) * Layout.interitemSpacing
        let width =  (containerWidth - interitemSpacing - Layout.insets.left - Layout.insets.right) / CGFloat(3)
        return CGSize(width: width, height: width)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: PostCell.self, for: self, at: index) as! PostCell
        let postViewModel = viewModel.items[index]
        cell.configure(with: postViewModel)
        return cell
    }
    
    override func didSelectItem(at index: Int) {
        onItemWasSelected?(index)
    }
}
