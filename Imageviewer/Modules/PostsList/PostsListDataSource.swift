//  
//  Created by Daniel Metzing on 02.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import IGListKit
import CoreData

final class PostsListDataSource: NSObject {
    
    private lazy var postsFetchResultController: NSFetchedResultsController<Post> = {
        let fetchRequest: NSFetchRequest<Post> = NSFetchRequest(entityName: "Post")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "createdTime", ascending: false)]
        let fetchResultController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                               managedObjectContext: coreDataStack.readContext,
                                                               sectionNameKeyPath: nil,
                                                               cacheName: nil)
        return fetchResultController
    }()
    
    private var fetchPostViewModels: [PostCellViewModel] {
        guard let posts = self.postsFetchResultController.fetchedObjects else { return [] }
        return posts.map { PostCellViewModel.makeViewModel(from: $0) }
    }
    
    var isEmpty: Bool {
        fetchPosts()
        guard let objects = self.postsFetchResultController.fetchedObjects else { return false }
        return objects.isEmpty
    }
    
    let coreDataStack: CoreDataStack
    
    var onDataSourceNeedsReload: (() -> Void)?
    var onPostViewModelWasSelected: ((PostCellViewModel) -> Void)?
    
    init(coreDataStack: CoreDataStack) {
        self.coreDataStack = coreDataStack
    }
    
    func fetchPosts() {
        try? self.postsFetchResultController.performFetch()
    }
}

// MARK: ListAdapterDataSource

extension PostsListDataSource: ListAdapterDataSource {
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return fetchPostViewModels
            .chunk(to: 3)
            .map { PostSectionControllerViewModel(items: $0) }
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        guard let viewModel = object as? PostSectionControllerViewModel else {
            fatalError("Unkown object being dequed")
        }
        
        let sectionController = PostsSectionController(viewModel: viewModel)
        sectionController.onItemWasSelected = { [weak self] index in
            let item = sectionController.viewModel.items[index]
            self?.onPostViewModelWasSelected?(item)
        }
        return sectionController
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}
