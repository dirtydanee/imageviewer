//  
//  Created by Daniel Metzing on 02.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import UIKit
import AlamofireImage

final class PostCell: UICollectionViewCell {
    
    private var imageView: UIImageView!
    private var infoStackView: UIStackView!
    private var likesLabel: UILabel!
    private var commentsLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpSubviews()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with viewModel: PostCellViewModel) {
        likesLabel.text = viewModel.likes
        commentsLabel.text = viewModel.comments
        imageView.contentMode = .center
        imageView.af_setImage(withURL:viewModel.imageURL, placeholderImage: Assets.placeholder.image) {  [weak imageView] response in
            guard let image = response.value, let request = response.request else { return }
            imageView?.af_imageDownloader?.imageCache?.add(image, for: request, withIdentifier: viewModel.imageURL.absoluteString)
            imageView?.contentMode = .scaleAspectFill
        }
    }
}

private extension PostCell {
    
    struct Layout {
        struct Stack {
            static let space: CGFloat = 4
            static let height: CGFloat = 12
            static let padding: CGFloat = 2 * Stack.space
        }
        
        struct Size {
            static let height: CGFloat = 12
        }
    }
    
    func setUpSubviews() {
        
        imageView = UIImageView(frame: .zero)
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(imageView)
        
        infoStackView = UIStackView(frame: .zero)
        infoStackView.translatesAutoresizingMaskIntoConstraints = false
        infoStackView.axis = .horizontal
        infoStackView.alignment = .fill
        infoStackView.distribution = .fill
        infoStackView.spacing = Layout.Stack.space
        infoStackView.backgroundColor = .black
        contentView.addSubview(infoStackView)
        
        let likeImageView = UIImageView(image: Assets.like.image)
        likeImageView.pinSize(.iconSize)
        infoStackView.addArrangedSubview(likeImageView)
        
        likesLabel = UILabel(frame: .zero)
        likesLabel.font = Theme.current.label.font
        likesLabel.textColor = Theme.current.label.textColor
        infoStackView.addArrangedSubview(likesLabel)
        
        let placeholderView = UIView(frame: .zero)
        placeholderView.pinSize(CGSize(width: Layout.Stack.padding, height: Layout.Stack.height))
        infoStackView.addArrangedSubview(placeholderView)
        
        let commentImageView = UIImageView(image: Assets.comment.image)
        commentImageView.pinSize(.iconSize)
        infoStackView.addArrangedSubview(commentImageView)
        
        commentsLabel = UILabel(frame: .zero)
        commentsLabel.font = Theme.current.label.font
        commentsLabel.textColor = Theme.current.label.textColor
        infoStackView.addArrangedSubview(commentsLabel)
        
        let activateables = [imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
                             imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
                             imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
                             imageView.bottomAnchor.constraint(equalTo: infoStackView.topAnchor, constant: -4),
                             infoStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 4),
                             infoStackView.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor),
                             infoStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -2),
                             infoStackView.heightAnchor.constraint(equalToConstant: Layout.Stack.height)]
         NSLayoutConstraint.activate(activateables)
    }
}
