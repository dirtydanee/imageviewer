//  
//  Created by Daniel Metzing on 05.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import IGListKit

final class FullScreenImageSectionController: ListSectionController {
    
    let viewModel: FullScreenImageCellViewModel
    
    init(viewModel: FullScreenImageCellViewModel) {
        self.viewModel = viewModel
        super.init()
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: collectionContext!.containerSize.height)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(of: FullScreenImageCell.self, for: self, at: index) as! FullScreenImageCell
        cell.configure(with: viewModel)
        return cell
    }
}
