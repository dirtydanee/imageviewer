//  
//  Created by Daniel Metzing on 05.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import UIKit

final class FullScreenImageCell: UICollectionViewCell {
    
    private var imageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpImageView()
        
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpImageView() {
        imageView = UIImageView(frame: .zero)
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(imageView)
        imageView.pinToSuperview()
    }
    
    func configure(with viewModel: FullScreenImageCellViewModel) {
        imageView.contentMode = .center
        imageView.af_setImage(withURL:viewModel.url, placeholderImage: Assets.placeholder.image) { [weak imageView] response in
            guard let image = response.value, let request = response.request else { return }
            imageView?.af_imageDownloader?.imageCache?.add(image, for: request, withIdentifier: viewModel.url.absoluteString)
            imageView?.contentMode = .scaleAspectFill
        }
    }
}
