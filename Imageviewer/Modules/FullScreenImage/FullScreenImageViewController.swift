//  
//  Created by Daniel Metzing on 05.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import UIKit
import IGListKit

class FullScreenImageViewController: UIViewController {
    
    private let viewModel: FullScreenImageViewModel
    private var collectionView: ListCollectionView!
    private var adapter: ListAdapter!
    private var blurEffectView: UIVisualEffectView!
    
    init(initialURL: URL, storeProvider: StoreProvider) {
        self.viewModel = FullScreenImageViewModel(initialURL: initialURL, storeProvider: storeProvider)
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpBlurBackground()
        setUpCollectionView()
        setUpViewModel()
        viewModel.displayImages()
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        blurEffectView.removeFromSuperview()
        setUpBlurBackground()
    }
}

private extension FullScreenImageViewController {
    
    func setUpView() {
        view.backgroundColor = .clear
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: Localized.actionClose, style: .done, target: self, action: #selector(didPressCloseButton(_:)))
    }
    
    func setUpBlurBackground() {
        blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: Theme.current.blur.effect))
        blurEffectView.frame = view.bounds
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(blurEffectView, at: 0)
    }
    
    func setUpCollectionView() {
        let layout = ListCollectionViewLayout(stickyHeaders: false, scrollDirection: .horizontal, topContentInset: 0, stretchToEdge: true)
        collectionView = ListCollectionView(frame: .zero, listCollectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.isPagingEnabled = true
        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        let squareSide = min(view.bounds.width, view.bounds.height)
        collectionView.pinSize(CGSize(width: squareSide, height: squareSide))
        collectionView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectionView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        let updater = ListAdapterUpdater()
        adapter = ListAdapter(updater: updater, viewController: self)
        adapter.collectionView = collectionView
    }
    
    func setUpViewModel() {
        viewModel.onImagesDisplay = { [weak self] (index, animated) in
            self?.adapter.dataSource = self?.viewModel.listDataSource
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                self?.collectionView.scrollToItem(at: IndexPath(item: 0, section: index), at: .centeredHorizontally, animated: animated)
            })
        }
    }
    
    @objc
    func didPressCloseButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}
