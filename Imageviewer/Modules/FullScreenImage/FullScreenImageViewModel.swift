//  
//  Created by Daniel Metzing on 05.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import Foundation
import IGListKit

final class FullScreenImageViewModel {
    
    private let storeProvider: StoreProvider
    private let initialURL: URL
    private let dataSource: FullScreenImageDataSource
    
    var listDataSource: ListAdapterDataSource {
        return dataSource
    }
    
    var onImagesDisplay: ((_ initialIndex: Int, _ animated: Bool) -> Void)?
    
    init(initialURL: URL, storeProvider: StoreProvider) {
        self.initialURL = initialURL
        self.storeProvider = storeProvider
        self.dataSource = FullScreenImageDataSource(coreDataStack: storeProvider.coreDataStack)
    }
    
    func displayImages() {
        dataSource.fetchPosts()
        guard let index = dataSource.index(of: initialURL) else { return }
        onImagesDisplay?(index, false)
    }
    
}
