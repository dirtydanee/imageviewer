//  
//  Created by Daniel Metzing on 05.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import CoreData
import IGListKit

final class FullScreenImageDataSource: NSObject {
    
    private lazy var postsFetchResultController: NSFetchedResultsController<Post> = {
        let fetchRequest: NSFetchRequest<Post> = NSFetchRequest(entityName: "Post")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "createdTime", ascending: false)]
        let fetchResultController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                               managedObjectContext: coreDataStack.readContext,
                                                               sectionNameKeyPath: nil,
                                                               cacheName: nil)
        return fetchResultController
    }()
    
    private var fetchPostViewModels: [FullScreenImageCellViewModel] {
        guard let posts = self.postsFetchResultController.fetchedObjects else { return [] }
        return posts.map { FullScreenImageCellViewModel.makeViewModel(from: $0) }
    }
    
    let coreDataStack: CoreDataStack
    
    var onDataSourceNeedsReload: (() -> Void)?
    var onPostViewModelWasSelected: ((PostCellViewModel) -> Void)?
    
    init(coreDataStack: CoreDataStack) {
        self.coreDataStack = coreDataStack
    }
    
    func fetchPosts() {
        try? self.postsFetchResultController.performFetch()
    }
    
    func index(of url: URL) -> Int? {
        guard let posts = self.postsFetchResultController.fetchedObjects else {
            fatalError("Posts should be fetched before getting index of url.")
        }
        
        for (index, post) in posts.enumerated() where post.images.contains(where: { URL(string: $0.path) == url }) {
            return index
        }
    
        return nil
    }
}

// MARK: ListAdapterDataSource

extension FullScreenImageDataSource: ListAdapterDataSource {
    
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return fetchPostViewModels
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        guard let viewModel = object as? FullScreenImageCellViewModel else {
            fatalError("Unkown object being dequed")
        }
        
        let sectionController = FullScreenImageSectionController(viewModel: viewModel)
        return sectionController
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }
}
