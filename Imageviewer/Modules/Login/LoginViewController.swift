//
//  Created by Daniel Metzing on 27.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//

import UIKit
import WebKit

protocol LoginViewControllerDelegate: class {
    func loginViewController(_ loginViewController: LoginViewController, didAuthenticateWithToken token: AccessToken)
}

final class LoginViewController: UIViewController, ActivityDisplayer, AlertDisplayer {
    
    weak var delegate: LoginViewControllerDelegate?
    
    private let webView: WKWebView
    private let viewModel: LoginViewModel
    
    init(apiClient: APIClient) {
        viewModel = LoginViewModel(apiClient: apiClient)
        webView = WKWebView(frame: .zero)
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpWebView()
        setUpViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.startLogin()
    }
}

// MARK: Set up

private extension LoginViewController {
    
    func setUpView() {
        view.backgroundColor = Theme.current.viewController.backgroundColor
    }
    
    func setUpWebView() {
        view.addSubview(webView)
        view.sendSubviewToBack(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.pinToSuperviewSafeArea()
        webView.navigationDelegate = viewModel
        webView.backgroundColor = .clear
    }
    
    func setUpViewModel() {
        viewModel.onLoadWebView = { [weak self] in
             self?.webView.load($0)
        }
        
        viewModel.onSetActivityState = { [weak self] in
            self?.status = $0
        }
        
        viewModel.onAuthenticated = { [weak self] in
            guard let self = self else { return }
            self.delegate?.loginViewController(self, didAuthenticateWithToken: $0)
        }
        
        viewModel.onShowAlert = { [weak self] alert, animated in
            self?.showAlert(alert, animated: animated)
        }
    }
}
