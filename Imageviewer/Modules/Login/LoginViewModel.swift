//
//  Created by Daniel Metzing on 27.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//

import Foundation
import WebKit
import Moya

final class LoginViewModel: NSObject {
    
    enum Error: Swift.Error {
        case unableToBuildAuthorizeRequest
        case unableToBuildAccessTokenRequest
    }
    
    enum State {
        case loading
        case userInteracting
        case authorized(code: String)
        case authenticated(token: AccessToken)
        case error(Swift.Error)
    }
    
    private var state: State = .loading {
        didSet {
            switch state {
            case .loading:
                onSetActivityState?(.loading)
            case .userInteracting:
                onSetActivityState?(.loaded)
            case .error(let error):
                onSetActivityState?(.loaded)
                onShowAlert?(UIAlertController.okAlert(for: error), true)
            case .authorized(let code):
                fetchAccessToken(with: code)
            case .authenticated(let token):
                onSetActivityState?(.loaded)
                onAuthenticated?(token)
            }
        }
    }
    
    var onLoadWebView: ((URLRequest) -> Void)?
    var onSetActivityState: ((ActivityStatus) -> Void)?
    var onShowAlert: ((UIAlertController, Bool) -> Void)?
    var onAuthenticated: ((AccessToken) -> Void)?
    
    let apiClient: APIClient
    
    init(apiClient: APIClient) {
        self.apiClient = apiClient
    }
    
    func startLogin() {
        state = .loading
        guard let request = makeInstagramAuthorizeRequest() else {
            state = .error(Error.unableToBuildAuthorizeRequest)
            return
        }
        onLoadWebView?(request)
    }
}

private extension LoginViewModel {
    
    func fetchAccessToken(with code: String) {
        let params: [String: Any] = [InstagramAPI.Keys.clientID.rawValue: InstagramAPI.Constants.Client.id,
                                     InstagramAPI.Keys.redirectURI.rawValue: InstagramAPI.Constants.redirectURI,
                                     InstagramAPI.Keys.clientSecret.rawValue: InstagramAPI.Constants.Client.secret,
                                     InstagramAPI.Keys.grantType.rawValue: InstagramAPI.Constants.Types.grant,
                                     InstagramAPI.Keys.code.rawValue: code]
        guard let data = params.asFormData else {
            state = .error(Error.unableToBuildAccessTokenRequest)
            return
        }
        
        apiClient.execute(target: .accessToken(data: data)) { [weak self] (result: Result<InstagramAPI.Response.Authenticate>) in
            switch result {
            case .success(let response):
                self?.state = .authenticated(token: response.accessToken)
            case .failure(let error):
                self?.state = .error(error)
            }
        }
    }
    
    func makeInstagramAuthorizeRequest() -> URLRequest? {
            let params: [String: Any] = [InstagramAPI.Keys.clientID.rawValue: InstagramAPI.Constants.Client.id,
                                         InstagramAPI.Keys.redirectURI.rawValue: InstagramAPI.Constants.redirectURI,
                                         InstagramAPI.Keys.responseType.rawValue: InstagramAPI.Constants.Types.response]
            let target = InstagramAPI.authorize(params: params)
            return try? MoyaProvider.defaultEndpointMapping(for: target).urlRequest()
    }
}

extension LoginViewModel: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        
        guard let url = webView.url else {
            return
        }
        
        let result = URLRedirectTransformer().unwrapResponse(from: url, verifyWithPrefix: InstagramAPI.Constants.redirectURI)
        
        switch result {
        case .success(let code):
            state = .authorized(code: code)
        case .failure(let error):
            if error is URLRedirectTransformer.ResponseError {
                state = .error(error)
            }
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        state = .userInteracting
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Swift.Error) {
        state = .error(error)
    }
}
