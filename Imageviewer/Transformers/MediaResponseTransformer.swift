//  
//  Created by Daniel Metzing on 02.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import CoreData

class MediaResponseTransformer {
    
    private let postsStore: PostsStore
    private let imageStore: RemoteImageStore
    
    init(postsStore: PostsStore, imageStore: RemoteImageStore) {
        self.postsStore = postsStore
        self.imageStore = imageStore
    }
    
    func insert(_ response: InstagramAPI.Response.UserRecentMedia, to context: NSManagedObjectContext) throws {
        for post in response.posts {
            let entity: Post = try postsStore.fetchOrCreateEntity(withIdentifier: post.id, in: context)
            entity.id = post.id
            entity.createdTime = post.createdTime
            entity.comments = Int64(post.comments)
            entity.likes = Int64(post.likes)
            
            let lowResimage: RemoteImage = try imageStore.fetchOrCreateEntity(withPath: post.lowResolutionURL.absoluteString, in: context)
            lowResimage.path = post.lowResolutionURL.absoluteString
            lowResimage.resolution = .low
            
            let standardResimage: RemoteImage = try imageStore.fetchOrCreateEntity(withPath: post.standardResolutionURL.absoluteString, in: context)
            standardResimage.path = post.standardResolutionURL.absoluteString
            standardResimage.resolution = .standard
        
            entity.images = [lowResimage, standardResimage]
        }
    }
}
