//  
//  Created by Daniel Metzing on 28.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import Foundation

struct URLRedirectTransformer {
    
    private enum Constants {
        static let code = "code"
        static let error = "error"
    }
    
    enum UnwrapError: Error {
        case unmatchingURLs
        case missingQueryItems
        case unkown
    }
    
    enum ResponseError: Error {
        case failedResponse(description: String?)
    }
    
    func unwrapResponse(from url: URL, verifyWithPrefix prefixURL: URL) -> Result<String> {
        guard let querylessURL = url.trimmingQueryParameters(), querylessURL == prefixURL else {
            return .failure(UnwrapError.unmatchingURLs)
        }
        
        guard let queryItems = URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems else {
            return .failure(UnwrapError.missingQueryItems)
        }

        if let errorItem = queryItems.first(where: { $0.name == Constants.error }) {
            return .failure(ResponseError.failedResponse(description: errorItem.value))
        }

        if let codeItem = queryItems.first(where: { $0.name == Constants.code }), let code = codeItem.value {
            return .success(code)
        }

        return .failure(UnwrapError.unkown)
    }
}
