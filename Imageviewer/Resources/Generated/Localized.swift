// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
internal enum Localized {
  /// Close
  internal static let actionClose = Localized.tr("Localizable", "action_close")
  /// Something is wrong with our bits. Please contact with our support.
  internal static let genericErrorMessage = Localized.tr("Localizable", "generic_error_message")
  /// Oops!
  internal static let genericErrorTitle = Localized.tr("Localizable", "generic_error_title")
  /// ok
  internal static let genericOk = Localized.tr("Localizable", "generic_ok")
  /// Posts
  internal static let postsTitle = Localized.tr("Localizable", "posts_title")
  /// Please try again later.
  internal static let unkownErrorMessage = Localized.tr("Localizable", "unkown_error_message")
  /// Unkown failure
  internal static let unkownErrorTitle = Localized.tr("Localizable", "unkown_error_title")
}
// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension Localized {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
