//
//  AppDelegate.swift
//  Imageviewer
//
//  Created by Daniel Metzing on 27.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        let appCoordinator = AppCoordinator(window: window!)
        appCoordinator.start()
        
        self.appCoordinator = appCoordinator
        
        return true
    }
}

