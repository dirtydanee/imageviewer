//  
//  Created by Daniel Metzing on 28.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import UIKit

protocol AlertDisplayer {
    func showAlert(_ controller: UIAlertController, animated: Bool)
}

extension AlertDisplayer where Self: UIViewController {
    
    func showAlert(_ controller: UIAlertController, animated: Bool = true) {
        present(controller, animated: animated, completion: nil)
    }
}
