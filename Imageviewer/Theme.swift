//  
//  Created by Daniel Metzing on 04.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import UIKit

struct Theme {
    
    struct Label {
        let font: UIFont
        let textColor: UIColor
        
        fileprivate static let light = Label(font: UIFont.systemFont(ofSize: 8), textColor: .black)
        fileprivate static let dark = Label(font: UIFont.systemFont(ofSize: 8), textColor: .white)
    }
    
    struct Cell {
        let backgroundColor: UIColor
        
        fileprivate static let light = Cell(backgroundColor: .white)
        fileprivate static let dark = Cell(backgroundColor: .black)
    }
    
    struct ViewController {
        let backgroundColor: UIColor
        
        fileprivate static let light = ViewController(backgroundColor: .lightGray)
        fileprivate static let dark = ViewController(backgroundColor: .darkGray)
    }
    
    struct NavigationBar {
        let alpha: CGFloat
        let translucent: Bool
        let tintColor: UIColor
        let barTintColor: UIColor?
        let barStyle: UIBarStyle
        let backgroundImage: UIImage?
        
        fileprivate static let light = NavigationBar(alpha: 1, translucent: true, tintColor: .black, barTintColor: nil, barStyle: .default, backgroundImage: nil)
        fileprivate static let dark = NavigationBar(alpha: 1, translucent: false, tintColor: .white, barTintColor: .black, barStyle: .black, backgroundImage: UIImage())
    }
    
    struct Blur {
        let effect: UIBlurEffect.Style
        
        fileprivate static let light = Blur(effect: .light)
        fileprivate static let dark = Blur(effect: .dark)
    }
    
    struct Activity {
        let backgroundColor: UIColor
        let style: UIActivityIndicatorView.Style
        
        fileprivate static let light = Activity(backgroundColor: .white, style: .gray)
        fileprivate static let dark = Activity(backgroundColor: .darkGray, style: .whiteLarge)
    }
    
    let label: Theme.Label
    let cell: Theme.Cell
    let viewController: Theme.ViewController
    let navigationBar: Theme.NavigationBar
    let blur: Theme.Blur
    let activity: Theme.Activity
    
    private static let light = Theme(label: .light, cell: .light, viewController: .light, navigationBar: .light, blur: .light, activity: .light)
    private static let dark = Theme(label: .dark, cell: .dark, viewController: .dark, navigationBar: .dark, blur: .dark, activity: .dark)
    
    static let current: Theme = .light
}

extension Theme {
    
    static func setUp(to theme: Theme) {
        setUpCollectionView(to: theme.cell)
        setUpNavigationBar(to: theme.navigationBar)
    }
    
    private static func setUpCollectionView(to theme: Theme.Cell) {
        let appearance = UICollectionViewCell.appearance()
        appearance.backgroundColor = theme.backgroundColor
        appearance.contentView.backgroundColor = theme.backgroundColor
    }
    
    private static func setUpNavigationBar(to theme: Theme.NavigationBar) {
        let appearance = UINavigationBar.appearance()
        appearance.alpha = theme.alpha
        appearance.barTintColor = theme.barTintColor
        appearance.tintColor = theme.tintColor
        appearance.setBackgroundImage(theme.backgroundImage, for: .default)
        appearance.shadowImage = theme.backgroundImage
        appearance.isTranslucent = theme.translucent
        appearance.barStyle = theme.barStyle
    }
}
