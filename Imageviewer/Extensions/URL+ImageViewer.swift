//  
//  Created by Daniel Metzing on 28.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import Foundation

extension URL {
    func trimmingQueryParameters() -> URL? {
        guard var components = URLComponents(url: self, resolvingAgainstBaseURL: false) else {
            return nil
        }
        components.query = nil
        return try? components.asURL()
    }
}
