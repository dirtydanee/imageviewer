//  
//  Created by Daniel Metzing on 04.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import UIKit

extension CGSize {
    static let iconSize = CGSize(width: 12, height: 12)
    static let largeSize = CGSize(width: 400, height: 400)
}
