//  
//  Created by Daniel Metzing on 28.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import UIKit

enum DisplayableError: Error {
    case generic
    case custom(title: String, message: String)
}
