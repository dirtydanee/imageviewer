//  
//  Created by Daniel Metzing on 28.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import UIKit

extension UIAlertController {
    
    static func okAlert(for error: Error, onOkPress: (() -> Void)? = nil) -> UIAlertController {
        let title: String
        let message: String
        switch error {
        case DisplayableError.generic:
            title = Localized.genericErrorTitle
            message = Localized.genericErrorMessage
        case let DisplayableError.custom(eTitle, eMessage):
            title = eTitle
            message = eMessage
        default:
            title = Localized.unkownErrorTitle
            message = Localized.unkownErrorMessage
        }
        
        let alertContoller = UIAlertController(title: title, message: message, preferredStyle: .alert)
         let okAction = UIAlertAction(title: Localized.genericOk, style: .default) { _ in
            onOkPress?()
        }
        alertContoller.addAction(okAction)
        return alertContoller
    }
}
