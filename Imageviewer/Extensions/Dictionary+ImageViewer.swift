//  
//  Created by Daniel Metzing on 31.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import Foundation

extension Dictionary where Key: ExpressibleByStringLiteral, Value: Any {
    
    var asFormData: Data? {
        let paramString = reduce(into: "") { (result, element) in result.append("\(element.key)=\(element.value)&") }.dropLast()
        guard let data = paramString.data(using: .utf8) else {
            return nil
        }
        
        return data
    }
    
}
