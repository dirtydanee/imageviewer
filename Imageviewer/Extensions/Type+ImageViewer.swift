//  
//  Created by Daniel Metzing on 28.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import Foundation

typealias AccessToken = String
typealias ClientID = String

enum Result<T> {
    case success(T)
    case failure(Swift.Error)
}
