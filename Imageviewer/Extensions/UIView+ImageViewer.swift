//  
//  Created by Daniel Metzing on 28.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import UIKit

extension UIView {
    
    @discardableResult
    func pinToSuperview() -> [NSLayoutConstraint]  {
        guard let superview = self.superview else {
            fatalError("Superview is not available!")
        }
        
         let activatables = [leadingAnchor.constraint(equalTo: superview.leadingAnchor),
                              trailingAnchor.constraint(equalTo: superview.trailingAnchor),
                              topAnchor.constraint(equalTo: superview.topAnchor),
                              bottomAnchor.constraint(equalTo: superview.bottomAnchor)]
        
        NSLayoutConstraint.activate(activatables)
        return activatables
    }
    
    @discardableResult
    func pinToSuperviewSafeArea() -> [NSLayoutConstraint] {
        
        guard let superview = self.superview else {
            fatalError("Superview is not available!")
        }
        
        if #available(iOS 11.0, *) {
            let activatables = [topAnchor.constraint(equalToSystemSpacingBelow: superview.safeAreaLayoutGuide.topAnchor, multiplier: 1.0),
                                 bottomAnchor.constraint(equalToSystemSpacingBelow: superview.safeAreaLayoutGuide.bottomAnchor, multiplier: 1.0),
                                 leadingAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.leadingAnchor),
                                 trailingAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.trailingAnchor)]
            NSLayoutConstraint.activate(activatables)
            return activatables
        } else {
            return pinToSuperview()
        }
    }
    
    @discardableResult
    func pinSize(_ size: CGSize) -> [NSLayoutConstraint] {
        let activatables = [ heightAnchor.constraint(equalToConstant: size.height),
                             widthAnchor.constraint(equalToConstant: size.width)]
        NSLayoutConstraint.activate(activatables)
        return activatables
    }
}
