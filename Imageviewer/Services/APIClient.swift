//  
//  Created by Daniel Metzing on 28.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import Moya

struct APIClient {
    
    private struct Keys {
        static let accessToken = "NjgxMzVCOTQzRjExNDIy"
    }
    
    private let provider: MoyaProvider<InstagramAPI>
    var accessToken: AccessToken? {
        get { return UserDefaults.standard.string(forKey: Keys.accessToken) }
        set { UserDefaults.standard.set(newValue, forKey:  Keys.accessToken)}
    }
    
    init(provider: MoyaProvider<InstagramAPI> = MoyaProvider<InstagramAPI>()) {
        self.provider = provider
    }
    
    func execute<DecodableResponse: Decodable>(target: InstagramAPI, decoder: JSONDecoder = InstagramAPI.defaultDecoder, completion: @escaping (Result<DecodableResponse>) -> Void) {
        provider.request(target) { result in
            switch result {
            case .success(let response):
                do {
                    let decodedResponse = try decoder.decode(DecodableResponse.self, from: response.data)
                    completion(.success(decodedResponse))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
