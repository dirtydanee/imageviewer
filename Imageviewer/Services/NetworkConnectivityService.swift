//  
//  Created by Daniel Metzing on 04.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import Alamofire

struct NetworkConnectivityService {
    
    static var isConnected: Bool {
        guard let networkManager = NetworkReachabilityManager() else { return false }
        return networkManager.isReachable
    }
}
