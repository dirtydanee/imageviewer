//  
//  Created by Daniel Metzing on 31.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import Foundation
import Moya

enum InstagramAPI {
    case authorize(params: [String: Any])
    case accessToken(data: Data)
    case userRecentMedia(token: AccessToken)
}

extension InstagramAPI: TargetType {
    
    var baseURL: URL {
        switch self {
        case .authorize, .accessToken:
            return URL(string: "https://api.instagram.com/oauth")!
        case .userRecentMedia:
            return URL(string: "https://api.instagram.com/v1")!
        }
    }
    
    var path: String {
        switch self {
        case .authorize:
            return "authorize"
        case .accessToken:
            return "access_token"
        case .userRecentMedia:
            return "users/self/media/recent"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .userRecentMedia, .authorize:
            return .get
        case .accessToken:
            return .post
        }
    }
    
    var sampleData: Data {
        switch self {
        case .userRecentMedia, .authorize, .accessToken:
            return Data() // TODO: Daniel Metzing - add tests
        }
    }
    
    var task: Task {
        switch self {
        case .authorize(let params):
            return .requestCompositeParameters(bodyParameters: [:], bodyEncoding: URLEncoding.httpBody, urlParameters: params)
        case .accessToken(let data):
            return .requestData(data)
        case .userRecentMedia(let token):
            return .requestCompositeParameters(bodyParameters: [:], bodyEncoding: URLEncoding.httpBody, urlParameters: ["access_token": token])
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .authorize, .accessToken:
            return nil
        case .userRecentMedia:
            return ["Content-type": "application/json"]
        }
    }
    
    var validationType: ValidationType {
        switch self {
        case .authorize:
            return .none
        case .userRecentMedia, .accessToken:
            return .successCodes
        }
    }
}

extension InstagramAPI {
    
    enum Keys: String {
        case clientID = "client_id"
        case clientSecret = "client_secret"
        case redirectURI = "redirect_uri"
        case responseType = "response_type"
        case grantType = "grant_type"
        case code = "code"
    }
    
    enum Constants {
        
        enum Client {
            static let id = "fa655afe68d945f292d3049e48907ba3"
            static let secret = "53c7a823c66f417e8d0b99596562e17a"
        }
        
        enum Types {
            static let grant = "authorization_code"
            static let response = "code"
        }
        
        static let redirectURI = URL(string: "https://imageviewer.com/redirect")!
    }
}

extension InstagramAPI {
    static let defaultDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
    
    static let defaultEncoder: JSONEncoder = {
        let decoder = JSONEncoder()
        decoder.keyEncodingStrategy = .convertToSnakeCase
        return decoder
    }()
}

extension InstagramAPI {
    struct Response {
        struct Authenticate: Decodable {
            let accessToken: String
        }
        
        struct UserRecentMedia: Decodable {
            
            struct Post {
                let id: String
                let createdTime: String
                let likes: Int
                let comments: Int
                let lowResolutionURL: URL
                let standardResolutionURL: URL
            }
            
            let posts: [Post]
            
            private enum CodingKeys: String, CodingKey {
                case data
            }
            
            private struct PostDescription: Decodable {
                
                struct URLDecodable: Decodable {
                    let url: URL
                }
                
                struct CountDecodable: Decodable {
                    let count: Int
                }
                
                let id: String
                let createdTime: String
                let images: [String: URLDecodable]
                let likes: CountDecodable
                let comments: CountDecodable
            }
            
            
            init(from decoder: Decoder) throws {
                let container = try decoder.container(keyedBy: CodingKeys.self)
                let descriptions = try container.decode([PostDescription].self, forKey: .data)
                self.posts = descriptions.compactMap {
                    guard let lrURL = $0.images["lowResolution"], let strURL = $0.images["standardResolution"] else {
                        return nil
                    }
                    return Post(id: $0.id, createdTime: $0.createdTime, likes: $0.likes.count, comments: $0.comments.count, lowResolutionURL: lrURL.url, standardResolutionURL: strURL.url)
                }
            }
        }
    }
    
}
