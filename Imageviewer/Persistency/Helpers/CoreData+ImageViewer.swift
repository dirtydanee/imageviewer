//
//  Created by Daniel Metzing on 02.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//

import CoreData

enum StorageType {
    case inMemory
    case SQLite(filename: String)

    var stringValue: String {
        switch self {
        case .inMemory:
            return NSInMemoryStoreType
        case .SQLite:
            return NSSQLiteStoreType
        }
    }
}

enum PersistencyError: Swift.Error {
    case failedToCreateEntity(name: String)
    case failedToDeleteEntity(name: String, withPredicate: String)
    case invalidEntityType(name: String)
    case modelNotFound(name: String)
    case urlNotFound(url: URL)
    case documentsURLNotFound
}
