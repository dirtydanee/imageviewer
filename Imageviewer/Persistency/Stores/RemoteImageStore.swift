//  
//  Created by Daniel Metzing on 02.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import CoreData

final class RemoteImageStore: DataStore {
    
    var entityName: String {
        return "RemoteImage"
    }
    
    func fetchOrCreateEntity(withPath path: String, in context: NSManagedObjectContext) throws -> RemoteImage {
        let predicate = NSPredicate(format: "path == %@", path)
        return try fetchOrCreateEntity(withName: self.entityName, in: context, usingPredicate: predicate)
    }}

