//  
//  Created by Daniel Metzing on 02.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import CoreData

protocol DataStore {
    var entityName: String { get }
    
    func createEntity(withName name: String,
                      in context: NSManagedObjectContext) throws -> NSEntityDescription
    
    func fetchEntity<T: NSFetchRequestResult>(withName name: String,
                                              in context: NSManagedObjectContext,
                                              usingPredicate predicate: NSPredicate,
                                              fetchLimit: Int) throws -> T?
    
    func fetchOrCreateEntity<T: NSFetchRequestResult>(withName name: String,
                                                      in context: NSManagedObjectContext,
                                                      usingPredicate predicate: NSPredicate) throws -> T
}

extension DataStore {
    
    func fetchOrCreateEntity<T: NSFetchRequestResult>(withName name: String, in context: NSManagedObjectContext, usingPredicate predicate: NSPredicate) throws -> T {
        if let entity: T = try fetchEntity(withName: name, in: context, usingPredicate: predicate) {
            return entity
        }
        return try createEntity(withName: name, in: context)
    }
    
    func createEntity<T>(withName name: String, in context: NSManagedObjectContext) throws -> T {
        
        guard let entityDescription = NSEntityDescription.entity(forEntityName: name, in: context) else {
            throw PersistencyError.failedToCreateEntity(name: name)
        }
        
        guard let entity = NSManagedObject(entity: entityDescription, insertInto: context) as? T else {
            throw PersistencyError.invalidEntityType(name: name)
        }
        
        return entity
    }
    
    func fetchEntity<T: NSFetchRequestResult>(withName name: String,
                                              in context: NSManagedObjectContext,
                                              usingPredicate predicate: NSPredicate,
                                              fetchLimit: Int = 1) throws -> T? {
        let fetchRequest = NSFetchRequest<T>(entityName: name)
        fetchRequest.predicate = predicate
        fetchRequest.fetchLimit = fetchLimit
        return try context.fetch(fetchRequest).first
    }
    
    func fetchEntities<T: NSFetchRequestResult>(withName name: String,
                                                in context: NSManagedObjectContext,
                                                usingPredicate predicate: NSPredicate? = nil) throws -> [T] {
        let fetchRequest = NSFetchRequest<T>(entityName: name)
        fetchRequest.predicate = predicate
        return try context.fetch(fetchRequest)
    }
    
    func fetchAllEntities<T: NSFetchRequestResult>(withName name: String, in context: NSManagedObjectContext) throws -> [T] {
        return try self.fetchEntities(withName: name, in: context, usingPredicate: nil)
    }
}

