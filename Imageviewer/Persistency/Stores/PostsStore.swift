//  
//  Created by Daniel Metzing on 02.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//

import CoreData

final class PostsStore: DataStore {
    
    var entityName: String {
        return "Post"
    }
    
    func fetchOrCreateEntity(withIdentifier identifier: String, in context: NSManagedObjectContext) throws -> Post {
        let predicate = NSPredicate(format: "id == %@", identifier)
        return try fetchOrCreateEntity(withName: self.entityName, in: context, usingPredicate: predicate)
    }
}
