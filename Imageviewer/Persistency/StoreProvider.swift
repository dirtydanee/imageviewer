//  
//  Created by Daniel Metzing on 05.11.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//  

import Foundation

final class StoreProvider {
    let postsStore = PostsStore()
    let imageStore = RemoteImageStore()
    let coreDataStack = try! CoreDataStack(modelName: "Post", storageType: .SQLite(filename: "Post.sqlite"))
}
