//  
//  Created by Daniel Metzing on 28.10.18.
//  Copyright © 2018 Daniel Metzing. All rights reserved.
//

@testable import Imageviewer
import XCTest

class URLTestCase: XCTestCase {
    let mockURL = URL(string: "https://imageviewer.io/test")!
    
    func addQueryItem(to url: URL, query: [String: String]) -> URL {
        var component =  URLComponents(url: mockURL, resolvingAgainstBaseURL: false)
        component?.queryItems = query.map { return URLQueryItem(name: $0.key, value: $0.value) }
        return component!.url!
    }
}

class URL_ImageViewerTests: URLTestCase {
    
    func testTrimmingNil() {
        let url: URL? = nil
        XCTAssertNil(url?.trimmingQueryParameters())
    }
    
    func testTrimmingURLWithNoQuery() {
        let trimmedURL = mockURL.trimmingQueryParameters()
        XCTAssertNotNil(trimmedURL)
        XCTAssertEqual(mockURL, trimmedURL)
    }
    
    func testTrimmingURLWithSingleQuery() {
        let mockURLWithQuery = addQueryItem(to: mockURL, query: ["foo": "bar"])
        let trimmedURL = mockURLWithQuery.trimmingQueryParameters()
        XCTAssertNotNil(trimmedURL)
        XCTAssertEqual(mockURL, trimmedURL)
    }
    
    func testTrimmingURLWithMultiQuery() {
        let mockURLWithQuery = addQueryItem(to: mockURL, query: ["foo": "bar", "bar": "foo", "fooBar": "barFoo"])
        let trimmedURL = mockURLWithQuery.trimmingQueryParameters()
        XCTAssertNotNil(trimmedURL)
        XCTAssertEqual(mockURL, trimmedURL)
    }
}
